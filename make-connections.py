#! /usr/bin/env python
import sys
import subprocess
import dns.resolver

if len(sys.argv) >= 4:
    res = dns.resolver.Resolver()
    res.nameservers = [sys.argv[3]]
else:
    res = dns.resolver

res.timeout = 10
res.lifetime = 10

host = sys.argv[1]
port = int(sys.argv[2])

ip = res.resolve(host, "AAAA")[0]
print(f"IP: {ip}")

main_key = res.resolve("localhost.loki", "CNAME")[0]
print(f"Our main key: {main_key}")

print()

for label in [1001, 30, 40, 50, 1001, 30, 40]:
    print(f"Reply for flowlabel {label}: ", end='', flush=True)
    result = subprocess.run(['./client', str(ip), str(port), str(label)], stdout=subprocess.PIPE)
    output = result.stdout.decode()
    print(output.strip())
