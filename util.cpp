#include "util.hpp"

#include <iostream>
#include <cstdio>
#include <cstring>

#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

using namespace std;

bool enable_flow_label(int sock)
{
    int on = 1;

    if (setsockopt(sock, IPPROTO_IPV6, IPV6_FLOWINFO_SEND, &on, sizeof(on)) < 0) {
        cerr << "setsockopt(IPV6_FLOWINFO_SEND): " << strerror(errno) << endl;
        return false;
    }
    if (setsockopt(sock, IPPROTO_IPV6, IPV6_FLOWINFO, &on, sizeof(on)) < 0) {
        cerr << "setsockopt(IPV6_FLOWINFO): \n" << strerror(errno) << endl;
        return false;
    }

    return true;
}

bool set_flow_label(int sock, struct sockaddr_in6 *sa6P, unsigned flowlabel)
{
    struct in6_flowlabel_req freq;

    flowlabel &= IPV6_FLOWINFO_FLOWLABEL;

    memset(&freq, 0, sizeof(freq));

    freq.flr_label = htonl(flowlabel);
    //printf("flow label: %d\n", flowlabel, flowlabel);
    freq.flr_action = IPV6_FL_A_GET;
    freq.flr_flags = IPV6_FL_F_CREATE;
    freq.flr_share = IPV6_FL_S_ANY;
    memcpy(&freq.flr_dst, &sa6P->sin6_addr, sizeof(freq.flr_dst));

    if (setsockopt(sock, IPPROTO_IPV6, IPV6_FLOWLABEL_MGR, &freq, sizeof(freq)) < 0) {
        cerr << "setsockopt(IPV6_FLOWLABEL_MGR): " << strerror(errno) << endl;
        return false;
    }

    sa6P->sin6_flowinfo = freq.flr_label;

    return true;
}

int get_flow_label(int sockfd)
{
    struct in6_flowlabel_req freq;
    memset(&freq, 0, sizeof(freq));
    unsigned size = sizeof(freq);
    freq.flr_action = IPV6_FL_A_GET;
    if (getsockopt(sockfd, IPPROTO_IPV6, IPV6_FLOWLABEL_MGR, &freq, &size) < 0) {
        cerr << "getsockopt(IPV6_FLOWLABEL_MGR): " << strerror(errno) << endl;
    }
    unsigned flowlabel = ntohl(freq.flr_label);
    //printf("local label %d share 0x%X linger %d expires %d\n", ntohl(freq.flr_label), freq.flr_share,
    //                                                     freq.flr_linger, freq.flr_expires);
    return flowlabel;
}

unsigned get_remote_flow_label(int sockfd)
{
    struct in6_flowlabel_req freq;
    memset(&freq, 0, sizeof(freq));
    unsigned size = sizeof(freq);
    freq.flr_action = IPV6_FL_A_GET;
    freq.flr_flags = IPV6_FL_F_REMOTE;
    getsockopt(sockfd, IPPROTO_IPV6, IPV6_FLOWLABEL_MGR, &freq, &size);
    unsigned flowlabel = ntohl(freq.flr_label);
    printf("remote label: %d\n", flowlabel);
    return flowlabel;
}
