all: client server

client: client.cpp util.cpp util.hpp
	g++ client.cpp util.cpp -o client

server: server.cpp util.cpp util.hpp
	g++ server.cpp util.cpp -o server

clean:
	rm client server
