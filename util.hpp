#include <linux/in6.h>

bool enable_flow_label(int sock);
bool set_flow_label(int sock, struct sockaddr_in6 *sa6P, unsigned flowlabel);
int get_flow_label(int sockfd);
unsigned get_remote_flow_label(int sockfd);
