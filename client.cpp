//
// Created by Marian Babik on 11/18/20.
//

#include "util.hpp"

#include <iostream>

#include <string.h>
#include <arpa/inet.h>
#include <errno.h>
#include <unistd.h>

using namespace std;

int simple_connection(string addr, int port, int flowlabel)
{
    int sockfd;
    struct sockaddr_in6 server_addr;
    char message[1000], server_reply[2000];

    if ((sockfd = socket(AF_INET6, SOCK_STREAM, IPPROTO_TCP)) < 0) {
        cerr << "could not create socket: " << strerror(errno) << endl;
        return 1;
    }

    server_addr.sin6_family = AF_INET6;
    inet_pton(AF_INET6, addr.c_str(), &server_addr.sin6_addr);
    server_addr.sin6_port = htons(port);

    enable_flow_label(sockfd);
    set_flow_label(sockfd, &server_addr, flowlabel);

    if (connect(sockfd, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0) {
        cerr << "connect() failed: " << strerror(errno) << endl;
        return 1;
    }

    //sprintf(message, "message with flowlabel %d\n", flowlabel);

    if (send(sockfd, message, strlen(message) + 1, 0) < 0) {
        cerr << "send failed: " << strerror(errno) << endl;
        return 1;
    }

    if (recv(sockfd, server_reply, 2000, 0) < 0) {
        cerr << "recv failed: " << strerror(errno) << endl;
        return 1;
    }

    unsigned got_flowlabel = get_flow_label(sockfd);
    if (got_flowlabel != flowlabel) {
        cerr << "could not set flowlabel (set " << flowlabel << ", got " << got_flowlabel << ")" << endl;
    }
    //get_remote_flow_label(sockfd);

    //cout << "server reply:" << endl;
    cout << server_reply << endl;

    close(sockfd);

    return 0;
}

int main(int argc, char *argv[])
{
    if (argc != 4) {
        cout << "Usage: " << argv[0] << " ipv6_addr port flowlabel" << endl;
        return 0;
    }

    string addr = argv[1];
    int port = stoi(argv[2]);
    int flowlabel = stoi(argv[3]);
    simple_connection(addr, port, flowlabel);

    return 0;
}
